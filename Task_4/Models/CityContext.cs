﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Task_4.Models
{
    public class CityContext : DbContext
    {

        public DbSet<City> Cities { get; set; }

    }
}