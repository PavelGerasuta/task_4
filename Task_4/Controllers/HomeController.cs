﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task_4.Models;
using System.Data.Entity;


namespace Task_4.Controllers
{
    public class HomeController : Controller
    {
        CityContext db = new CityContext();

        public ActionResult Index()
        {

            var cities = db.Cities;



            return View(cities.ToList());
        }


        [HttpGet]
        public ActionResult EditCity(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            City city = db.Cities.Find(id);
            if (city != null)
            {
                return View(city);
            }
            return HttpNotFound();
        }



        [HttpPost]
        public ActionResult EditCity(City city)
        {
            db.Entry(city).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult CreateCity()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CreateCity(City city)
        {
            db.Cities.Add(city);
            db.SaveChanges();

            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult DeleteCity(int id)
        {
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        [HttpPost, ActionName("DeleteCity")]
        public ActionResult DeleteConfirmed(int id)
        {
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            db.Cities.Remove(city);
            db.SaveChanges();
            return RedirectToAction("Index");
        }




    }
}